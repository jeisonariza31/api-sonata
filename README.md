## Configuración, instalación y ejecución.

1. Crear un entorno virtual `python -m venv .env`
   Nota: `.env` es el nombre que tomara el entorno virtual, se aconseja asignar un nombre terminado en `.env`.

2. Luego de esto activar el entorno virtual, `source .env/bin/activate` o `.env\Scripts\activate.bat`

3. Una vez activado el entorno virtual instalar las dependencias enumeradas en `requirements.txt`

```shell
pip install -r requirements.txt
```

4. (Opcional) Migrar el modelo relacional a una base de datos relacional. `python manage.py migrate`

5. Una vez configurada la aplicación, levantar los servicios de la misma.

``` shell
python manage.py migrate
python manage.py createsuperuser --username produccion --email admin@info.com
python manage.py runserver
```

``` shell
http://127.0.0.1:8000/es/admin/login/
```

``` python
# build
heroku apps:create {name_application}
heroku config:set     DISABLE_COLLECTSTATIC=1 -a {name_application}
heroku apps:stacks:set heroku-20 -a {name_application}
heroku git:remote -a {name_application}

# deploy
git push heroku main
heroku run python manage.py migrate

# log
heroku logs --tail --app {name_application}

# list apps
heroku apps
heroku apps:info -a {name_application}
```

``` python
# database - dev
heroku pg:psql postgresql-objective-91490 --app sbd-arrecife-backend
heroku pg:info
```
