"""arrecife URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
#from rest_framework.schemas import get_schema_view

schema_view = get_schema_view(
   openapi.Info(
      title="Documentation API",
      default_version='v1',
      description="Test description",
      terms_of_service="",
      contact=openapi.Contact(email="jeison.ariza@sbd.com.co"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/role', include('apps.role.urls')),
    path('api/v1/login', include('apps.login.urls')),
    path('api/v1/user', include('apps.user.urls')),
    path('api/v1/client', include('apps.client.urls')),
    path('api/v1/project', include('apps.project.urls')),
    path('api/v1/project', include('apps.ContactProject.urls')),
    path('api/v1/project', include('apps.task.urls')),
    path('api/v1/project', include('apps.activity.urls')),
    path('api/v1/project', include('apps.ExecutionActivity.urls')),
    path('api/v1/RolProject', include('apps.RolProject.urls')),
    path('api/v1/phase', include('apps.phase.urls')),
    path(r'swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path(r'redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    #path('schema',get_schema_view(
    #    title='api',
    #    description='documentation api',
    #    version='v1'
    #), name='schema' )
]
