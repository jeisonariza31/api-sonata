from .models import *
from rest_framework import serializers 

class PhaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = fase
        fields = '__all__'