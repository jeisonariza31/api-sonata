from django.urls import path
from .Views.phase import *
from .Views.edit import *

urlpatterns = [
    path('', phase.as_view(), name='phase'),
    path('/<int:id>', EditPhase.as_view(), name='EditPhase'),
]