from django.shortcuts import render

from apps.phase.models import *
from apps.phase.serializer import *

from rest_framework import status, generics
from rest_framework.response import Response
# Create your views here.

class phase(generics.GenericAPIView):
    queryset = fase.objects.all()
    serializer_class=PhaseSerializer

    def get (self, request):
        phase = fase.objects.all()
        serializer = PhaseSerializer(phase, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post (self, request):
        inf = request.data.copy()
        serializers = PhaseSerializer(data = inf)
        if serializers.is_valid():
            serializers.save()
            return Response(inf,status=status.HTTP_200_OK)
        else:
            return Response(serializers.errors, status=status.HTTP_400_BAD_REQUEST)