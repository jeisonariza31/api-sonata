from django.shortcuts import render

from apps.phase.models import *
from apps.phase.serializer import *

from rest_framework import status, generics
from rest_framework.response import Response

class EditPhase(generics.GenericAPIView):
    serializer_class= PhaseSerializer
    def get(self, request, id):
        phase = fase.objects.get(id = id)  
        serializer = PhaseSerializer(phase)
        return Response(serializer.data, status=status.HTTP_200_OK)
        
    def put(self, request, id):
            phase = fase.objects.get(id = id)
            serializer = PhaseSerializer(phase, request.data)  
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)