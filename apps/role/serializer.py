from .models import *
from rest_framework import serializers 

class RolSerializer(serializers.ModelSerializer):
    class Meta:
        model = rol
        fields = '__all__'