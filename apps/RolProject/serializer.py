from .models import *
from rest_framework import serializers

class RolProyectoSerializer(serializers.ModelSerializer):
    class Meta:
        model = RolProyecto
        fields = '__all__'
