from django.shortcuts import render

from apps.RolProject.models import *
from apps.RolProject.serializer import *

from rest_framework import status, generics
from rest_framework.response import Response
# Create your views here.

class RolProject(generics.GenericAPIView):
    serializer_class=RolProyectoSerializer
    queryset = RolProyecto.objects.all() 
    def get (self, request):
        rolP = RolProyecto.objects.all()
        serializer = RolProyectoSerializer(rolP, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)