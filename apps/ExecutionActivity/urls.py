from django.urls import path
from .Views.cronograma import *

urlpatterns = [
    path('/<int:project>/task/<int:task>/activities/<int:activity>/schedule', cronograma.as_view(), name='cronograma'),
]