from django.db import models

from apps.activity.models import *
from apps.user.models import *

class cronograma (models.Model):
    id = models.AutoField(primary_key = True)
    fecha = models.DateField()
    horas_plan = models.IntegerField()
    horas_eje = models.IntegerField( blank= True)
    comentario = models.TextField(null = True, blank = True)
    usuario_creacion = models.ForeignKey(usuario, on_delete = models.CASCADE, related_name='creacionCro')
    ActividadCro = models.ForeignKey(actividad, on_delete = models.CASCADE)
    fecha_cracion = models.DateField()

    def __str__(self):
        return self.horas_eje