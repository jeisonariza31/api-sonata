from django.shortcuts import render

# Create your views here.
from apps.ExecutionActivity.models import cronograma as CronogramaModel
from apps.ExecutionActivity.serializer import *

from rest_framework import status, generics
from rest_framework.response import Response
from datetime import datetime

class cronograma (generics.GenericAPIView):
    queryset = CronogramaModel.objects.all()
    serializer_class= NewCronogramaSerializer

    def get (self, request, project, task, activity):
        schedule = CronogramaModel.objects.all()
        serializer = CronogramaSerializer(schedule, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)