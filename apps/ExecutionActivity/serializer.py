from .models import *

from rest_framework import serializers

from apps.activity.serializer import *
from apps.user.serializer import *


""" default serializer """

class CronogramaSerializer(serializers.ModelSerializer):
    class Meta:
        model = cronograma
        fields = '__all__'

class NewCronogramaSerializer(serializers.ModelSerializer):
    fecha = serializers.DateField()
    horas_plan = serializers.IntegerField()
    usuario_creacion = models.ForeignKey(usuario, on_delete = models.CASCADE)

    class Meta:
        model = cronograma
        fields = ['fecha', 'horas_plan', 'usuario_creacion']
