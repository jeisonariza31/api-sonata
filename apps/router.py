class Router(object):

    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'user' or model._meta.app_label == 'project' or model._meta.app_label == 'login' or model._meta.app_label == 'role' or model._meta.app_label == 'phase' or model._meta.app_label == 'RolProject' or model._meta.app_label == 'task' or model._meta.app_label == 'activity' or model._meta.app_label == 'ContactProject' or model._meta.app_label == 'ExecutionActivity':
            return 'business'
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label == 'user' or model._meta.app_label == 'project' or model._meta.app_label == 'login' or model._meta.app_label == 'role' or model._meta.app_label == 'phase' or model._meta.app_label == 'RolProject' or model._meta.app_label == 'task'  or model._meta.app_label == 'activity' or model._meta.app_label == 'ContactProject' or model._meta.app_label == 'ExecutionActivity':
            return 'business'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        if obj1._meta.app_label == 'user' or obj2._meta.app_label == 'project' or obj2._meta.app_label == 'login' or obj2._meta.app_label == 'role' or obj2._meta.app_label == 'phase' or obj2._meta.app_label == 'RolProject' or obj2._meta.app_label == 'task' or obj2._meta.app_label == 'activity' or obj2._meta.app_label == 'ContactProject' or obj2._meta.app_label == 'ExecutionActivity':
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if app_label == 'user' or  app_label == 'project' or  app_label == 'role' or  app_label == 'login' or  app_label == 'phase' or  app_label == 'RolProject' or app_label == 'task' or  app_label == 'activity' or app_label == 'ContactProject' or app_label == 'ExecutionActivity':
            return db == 'business'
        return None
