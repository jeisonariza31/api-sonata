from django.urls import path
from .Views.login import *
from .Views.RecoverPassword import *

urlpatterns = [
    path('', login.as_view(), name = 'login'),
    path('/recover',RecoverPassword.as_view(), name = 'RecoverPassword')
]