from .models import *
from rest_framework import serializers 
from apps.user.serializer import *

class LoginSerializer(serializers.ModelSerializer):
    correo = serializers.CharField(max_length = 200, min_length=10 )
    contrasena = serializers.CharField(max_length=200, min_length=8)
    class Meta:
        model = usuario
        fields = ['correo','contrasena']

class RecoverPasswordSerializer(serializers.ModelSerializer):
    correo = serializers.CharField(max_length = 200, min_length=10 )

    class Meta:
        model = usuario
        fields = ['correo']

class ChangePasswordSerializer(serializers.ModelSerializer):
    contrasena = serializers.CharField(max_length=100, min_length=8)

    class Meta:
        model = usuario
        fields = ['contrasena']