from django.shortcuts import render

from apps.activity.models import *
from apps.activity.serializer import *

from rest_framework import status, generics
from rest_framework.response import Response
from datetime import datetime

class EditActivity(generics.GenericAPIView):
    serializer_class = EditActivitySerializer
    queryset = actividad.objects.all()

    def get (self, request, project, task, activity):
        activity = actividad.objects.get(id = activity)
        serializer = EditActivitySerializer(activity)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put (self, request, project, task, activity):
        activity = actividad.objects.get(id = activity)
        serializer = EditActivitySerializer(activity, request.data) 
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)