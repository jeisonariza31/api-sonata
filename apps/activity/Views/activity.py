from django.shortcuts import render

from apps.activity.models import *
from apps.activity.serializer import *

from rest_framework import status, generics
from rest_framework.response import Response
from datetime import datetime

class activity(generics.GenericAPIView):
    serializer_class = NewActivitySerializer
    queryset = actividad.objects.all()

    def get (self, request, project, task):
        task = actividad.objects.filter(tarea = task)
        serializer = ActivitySerializer(task, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post (self, request, project, task):
        inf = request.data.copy()
        add ={
            'tarea':task,
            'fecha_creacion': datetime.now(),
        }
        inf.update(add)
        serializers = ActivitySerializer(data = inf)
        if serializers.is_valid():
            serializers.save()
            return Response(inf,status=status.HTTP_200_OK)
        else:
            return Response(serializers.errors, status=status.HTTP_400_BAD_REQUEST)