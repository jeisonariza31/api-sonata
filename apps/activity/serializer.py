from .models import *
from rest_framework import serializers
from apps.user.serializer import *


""" default serializer """

class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = actividad
        fields = '__all__'

""" custom serializer """

class NewActivitySerializer(serializers.ModelSerializer):
    nombre = serializers.CharField(max_length = 100)
    horas_plan = serializers.IntegerField()
    consultor = models.ForeignKey(usuario, on_delete = models.CASCADE)
    usuario_creacion = models.ForeignKey(usuario, on_delete = models.CASCADE)

    class Meta:
        model = actividad
        fields = ['nombre', 'horas_plan', 'consultor', 'usuario_creacion']

class EditActivitySerializer(serializers.ModelSerializer):
    nombre = serializers.CharField(max_length = 100)
    horas_plan = serializers.IntegerField()
    consultor = models.ForeignKey(usuario, on_delete = models.CASCADE)

    class Meta:
        model = actividad
        fields = ['nombre', 'horas_plan', 'consultor']