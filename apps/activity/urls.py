from django.urls import path
from .Views.activity import *
from .Views.edit import *

urlpatterns = [
    path('/<int:project>/task/<int:task>/activities', activity.as_view(), name='actividades'),
    path('/<int:project>/task/<int:task>/activities/<int:activity>', EditActivity.as_view(), name='edit'),
]