from django.db import models
from apps.user.models import *
from apps.task.models import *

# Create your models here.
class actividad(models.Model):
    id = models.AutoField(primary_key = True)
    nombre = models.CharField(max_length = 100)
    horas_plan = models.IntegerField()
    consultor = models.ForeignKey(usuario, on_delete = models.CASCADE, blank=True, related_name='consultor')
    usuario_creacion = models.ForeignKey(usuario, on_delete = models.CASCADE)
    fecha_creacion = models.DateField(auto_now = True)
    tarea = models.ForeignKey(tarea, on_delete = models.CASCADE)

    def __str__(self):
        return self.nombre



    

