from django.urls import path
from .Views.user import *
from .Views.edit import *
from .Views.ActiveInactive import *

urlpatterns = [
    path('', user.as_view(), name = 'user'),
    path('/<int:id>', EditUser.as_view(), name = 'edit'),
    path('/inactive-active/<int:id>', ActiveInactive.as_view(), name = 'inactive'),
]
