from .models import *
from rest_framework import serializers 

""" default serializer """

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = usuario
        fields = '__all__'


""" custom serializers """

class NewUserSerializer(serializers.ModelSerializer):
    nombre = serializers.CharField(max_length = 100)
    correo = serializers.EmailField(max_length = 200)
    rol = models.ForeignKey(rol, on_delete=models.CASCADE, blank = False)
    class Meta:
        model = usuario
        fields = ['nombre','correo','rol']

class InactiveSerializer(serializers.ModelSerializer):
    estado = serializers.CharField(max_length = 20)

    class Meta:
        model = usuario
        fields = ['estado']
