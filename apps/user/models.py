from django.db import models
from apps.role.models import *

# Create your models here.
class usuario (models.Model):
    id = models.AutoField(primary_key = True)
    nombre = models.CharField(max_length = 100)
    correo = models.EmailField(max_length = 200, unique = True)
    contrasena = models.CharField(max_length = 200)
    estado = models.CharField(max_length = 20)
    fecha_creacion = models.DateField(auto_now = True)
    rol = models.ForeignKey(rol, on_delete=models.CASCADE, blank = False)

    def __str__(self):
        return self.nombre