from django.shortcuts import render

from apps.user.models import *
from apps.user.serializer import *

from rest_framework import status, generics
from rest_framework.response import Response

class ActiveInactive(generics.GenericAPIView):
    serializer_class=InactiveSerializer
    def patch(self, request, id):
        user = usuario.objects.get(id = id)
        if user.estado == "ACTIVO":  
            inf = {
                'estado' : 'INACTIVO'
            }
            serializer = InactiveSerializer(user,data = inf, partial=True)

            if serializer.is_valid():
                print (inf)
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            else: 
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        elif user.estado == "INACTIVO":  
            inf = {
                'estado' : 'ACTIVO'
            }
            serializer = InactiveSerializer(user,data = inf, partial=True)
            if serializer.is_valid():
                print (inf)
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
                
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
