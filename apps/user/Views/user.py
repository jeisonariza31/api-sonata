from django.shortcuts import render

from apps.user.models import *
from apps.user.serializer import *
from apps.client.Views.client import SendEmail

from rest_framework import status, generics
from rest_framework.response import Response
from datetime import datetime
from random import choice
import hashlib

class user(generics.GenericAPIView):
    queryset = usuario.objects.all()
    serializer_class=NewUserSerializer 

    def get (self, request):
        users = usuario.objects.exclude(rol=5)
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(sel, request):
            longitud = 10
            valores = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ<=>@#%&+"
            password = ""
            password = password.join([choice(valores) for i in range(longitud)])
            PasswordEncode = password.encode('utf8')
            encript = hashlib.sha512(PasswordEncode).hexdigest()

            inf = request.data.copy()
            add = {
                'estado' : 'ACTIVO',
                'FECHA_CREACION': datetime.now(),
                'contrasena':encript
            }
            inf.update(add)
            serializers = UserSerializer(data = inf)
            if serializers.is_valid():
                serializers.save()
                user = inf.get('nombre')
                email = inf.get('correo')
                password = password
                SendEmail(user,email,password)
                return Response(serializers.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializers.errors, status=status.HTTP_400_BAD_REQUEST)