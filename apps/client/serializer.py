from .models import *
from apps.user.models import *
from apps.user.serializer import *
from rest_framework import serializers 

""" default serializer """


class ClientSerializer(serializers.ModelSerializer):
    nombre = serializers.CharField(max_length = 100)
    correo = serializers.EmailField(max_length = 200)
    class Meta:
        model = usuario
        fields = ['nombre','correo']

class EditClientSerializer(serializers.ModelSerializer):
    nombre = serializers.CharField(max_length = 100)
    correo = serializers.EmailField(max_length = 200)
    estado = serializers.CharField(max_length = 20)
    class Meta:
        model = usuario
        fields = ['nombre','correo','estado']