from django.urls import path
from .Views.client import *
from .Views.edit import *

urlpatterns = [
    path('', client.as_view(), name = 'list'),
    path('/<int:id>', EditClient.as_view(), name = 'edit'),
    #path('/inactive-active/<int:id>', ActiveInactive.as_view(), name = 'inactive'),
]