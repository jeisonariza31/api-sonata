from django.shortcuts import render
from django.conf import settings

from apps.client.models import *
from apps.client.serializer import *

from rest_framework import status, generics
from rest_framework.response import Response
from datetime import datetime
from random import choice
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives

def SendEmail(user,email,password):
    context = {'email':email,'user':user,'password':password}
    template = get_template('email.html')
    contend = template.render(context)

    email = EmailMultiAlternatives(
        'REGISTRO EN ARRECIFE',
        'CREDENCIALES',
        settings.EMAIL_HOST_USER,
        [email],
        cc=[],
    )

    email.attach_alternative(contend,'text/html')
    email.send()

class client(generics.GenericAPIView):
    queryset = usuario.objects.all()
    serializer_class=ClientSerializer

    def get (self, request):
        users = usuario.objects.filter(rol = 5)
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(sel, request):
            longitud = 10
            valores = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ<=>@#%&+"
            password = ""
            password = password.join([choice(valores) for i in range(longitud)])

            inf = request.data.copy()
            add = {'estado':'ACTIVO',
                'fecha_creacion':datetime.now(),
                'contrasena':password,
                'rol':'5'
            }
            inf.update(add)
            serializers = UserSerializer(data = inf)
            if serializers.is_valid():
                serializers.save()
                user = inf.get('nombre')
                email = inf.get('correo')
                password = inf.get('contrasena')
                SendEmail(user,email,password)
                return Response(serializers.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializers.errors, status=status.HTTP_400_BAD_REQUEST)