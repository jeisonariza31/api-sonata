from django.shortcuts import render

from apps.client.models import *
from apps.client.serializer import *

from rest_framework import status, generics
from rest_framework.response import Response

class EditClient(generics.GenericAPIView):
    serializer_class= UserSerializer
    serializer_class=UserSerializer

    def get(self, request, id):
        user = usuario.objects.get(id = id, rol=5) 
        serializer = UserSerializer(user)
        return Response(serializer.data, status=status.HTTP_200_OK)
        
    def put(self, request, id):
            user = usuario.objects.get(id = id, rol=5)
            serializer = UserSerializer(user, request.data)  
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
