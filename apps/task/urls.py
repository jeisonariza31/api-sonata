from django.urls import path
from .Views.task import *
from .Views.edit import *
from .Views.AddUser import *

urlpatterns = [
    path('/<int:project>/task', task.as_view(), name='tareas'),
    path('/<int:project>/task/<int:id>', EditTask.as_view(), name='edit'),
    path('/<int:project>/task/<int:tarea>/user', AddUser.as_view(), name='users')
]