from django.shortcuts import render

# Create your views here.
from apps.task.models import *
from apps.task.serializer import *

from rest_framework import status, generics
from rest_framework.response import Response
from datetime import datetime

class AddUser(generics.GenericAPIView):
    serializer_class = NewUserTask
    queryset = UsuariosTarea.objects.all()

    def get(self, request, project, tarea):
        users = UsuariosTarea.objects.filter(tarea = tarea)
        serializer =  UserTaskSerializer(users, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, project, tarea):
        inf = request.data.copy()
        add ={
            'tarea':tarea,
            'fecha_creacion': datetime.now(),
        }
        inf.update(add)
        serializers = UserTaskSerializer(data = inf)
        if serializers.is_valid():
            serializers.save()
            return Response(inf,status=status.HTTP_200_OK)
        else:
            return Response(serializers.errors, status=status.HTTP_400_BAD_REQUEST)