from django.shortcuts import render

# Create your views here.
from apps.task.models import *
from apps.task.serializer import *

from rest_framework import status, generics
from rest_framework.response import Response

class EditTask(generics.GenericAPIView):
    serializer_class = EditTaskSerializer
    queryset = tarea.objects.all()

    def get (self, request, project, id):
        task = tarea.objects.get(id = id)
        serializer = TaskSerializer(task)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, project ,id):
            task = tarea.objects.get(id = id)
            serializer = EditTaskSerializer(task, request.data)  
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)