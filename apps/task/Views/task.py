from django.shortcuts import render

# Create your views here.
from apps.task.models import *
from apps.task.serializer import *

from rest_framework import status, generics
from rest_framework.response import Response
from datetime import datetime

class task(generics.GenericAPIView):
    serializer_class = NewTaskSerializer
    queryset = tarea.objects.all()
    def get (self, request, project):
        task = tarea.objects.filter(proyecto = project)
        serializer = TaskSerializer(task, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post (self, request, project):
        inf = request.data.copy()
        add ={
            'estado':'En espera',
            'proyecto':project,
            'fecha_creacion': datetime.now(),
        }
        inf.update(add)
        serializers = TaskSerializer(data = inf)
        if serializers.is_valid():
            serializers.save()
            return Response(inf,status=status.HTTP_200_OK)
        else:
            return Response(serializers.errors, status=status.HTTP_400_BAD_REQUEST)