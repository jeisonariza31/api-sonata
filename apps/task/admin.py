from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(tarea)
admin.site.register(UsuariosTarea)
admin.site.register(ObservacionesTareas)