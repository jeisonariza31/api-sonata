# Generated by Django 3.1.7 on 2021-03-16 02:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('user', '0002_auto_20210301_1720'),
        ('project', '0005_auto_20210315_0949'),
    ]

    operations = [
        migrations.CreateModel(
            name='tarea',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=50)),
                ('descripcion', models.TextField(max_length=1000)),
                ('fecha_inicio', models.DateField()),
                ('fecha_fin', models.DateField(blank=True)),
                ('horas_plan', models.IntegerField()),
                ('estado', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='UsuariosTarea',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('fecha_creacion', models.DateField(auto_now_add=True)),
                ('tarea', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='task.tarea')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user.usuario')),
            ],
        ),
        migrations.AddField(
            model_name='tarea',
            name='consultores',
            field=models.ManyToManyField(related_name='consultores', through='task.UsuariosTarea', to='user.usuario'),
        ),
        migrations.AddField(
            model_name='tarea',
            name='proyecto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='project.proyecto'),
        ),
        migrations.AddField(
            model_name='tarea',
            name='usuario_creacion',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user.usuario'),
        ),
        migrations.CreateModel(
            name='ObservacionesTareas',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('observacion', models.TextField(max_length=1000)),
                ('fecha', models.DateField(auto_now_add=True)),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user.usuario')),
            ],
        ),
    ]
