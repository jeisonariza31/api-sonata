from django.db import models
from apps.user.models import *
from apps.project.models import *

# Create your models here.

class tarea(models.Model):
    id = models.AutoField(primary_key = True)
    nombre = models.CharField(max_length = 50)
    descripcion = models.TextField(max_length = 1000)
    fecha_inicio = models.DateField()
    fecha_fin = models.DateField(blank = True)
    horas_plan = models.IntegerField()
    estado = models.CharField(max_length = 30)
    proyecto = models.ForeignKey(proyecto, on_delete = models.CASCADE)
    usuario_creacion = models.ForeignKey(usuario, on_delete = models.CASCADE)
    fecha_creacion = models.DateField(auto_now_add = True)
    consultores = models.ManyToManyField(usuario, through='UsuariosTarea', related_name = 'consultores')

    def __str__(self):
        return self.nombre

class UsuariosTarea(models.Model):
    id = models.AutoField(primary_key = True)
    tarea = models.ForeignKey(tarea, on_delete = models.CASCADE)
    usuario = models.ForeignKey(usuario, on_delete = models.CASCADE)
    fecha_creacion = models.DateField(auto_now_add = True)


class ObservacionesTareas(models.Model):
    id = models.AutoField(primary_key = True)
    observacion = models.TextField(max_length = 1000)
    fecha = models.DateField(auto_now_add = True)
    usuario = models.ForeignKey(usuario, on_delete = models.CASCADE)