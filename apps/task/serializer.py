from .models import *
from rest_framework import serializers

"""defaul serializer"""
class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = tarea
        fields = '__all__'

class UserTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = UsuariosTarea
        fields = '__all__'

class ObservationsTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = ObservacionesTareas
        fields = '__all__'

"""custom serializer"""

class NewTaskSerializer(serializers.ModelSerializer):
    nombre = serializers.CharField(max_length = 50)
    descripcion = serializers.CharField(style={'base_template': 'textarea.html'},max_length = 1000)
    fecha_inicio = serializers.DateField()
    fecha_fin = serializers.DateField()
    horas_plan = serializers.IntegerField()
    usuario_creacion = models.ForeignKey(usuario, on_delete = models.CASCADE)

    class Meta:
        model = tarea
        fields = ['nombre', 'descripcion', 'fecha_inicio', 'fecha_fin', 'horas_plan', 'usuario_creacion']

class EditTaskSerializer(serializers.ModelSerializer):
    nombre = serializers.CharField(max_length = 50)
    descripcion = serializers.CharField(style={'base_template': 'textarea.html'},max_length = 1000)
    fecha_inicio = serializers.DateField()
    fecha_fin = serializers.DateField()
    horas_plan = serializers.IntegerField()
    estado = serializers.CharField(max_length = 30)

    class Meta:
        model = tarea
        fields = ['nombre', 'descripcion', 'fecha_inicio', 'fecha_fin', 'horas_plan', 'estado']

class NewUserTask(serializers.ModelSerializer):
    usuario = models.ForeignKey(usuario, on_delete = models.CASCADE)

    class Meta:
        model = UsuariosTarea
        fields = ['usuario']