from django.shortcuts import render

# Create your views here.
from apps.project.models import *
from apps.project.serializer import *

from rest_framework import status, generics
from rest_framework.response import Response
from datetime import datetime

class ProjectListAdmin(generics.GenericAPIView):
    queryset = proyecto.objects.all()
    serializer_class= ProjectSerializer
    def get(self, request):
            projects = proyecto.objects.all()
            serializer = ProyectoSerializer(projects, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        inf = request.data.copy()
        add ={
            'estado':'En espera',
            'estado_entrega':'A tiempo',
            'fecha_creacion': datetime.now(),
            'fase':1
        }
        inf.update(add)
        print(inf)
        serializers = ProyectoSerializer(data = inf)
        if serializers.is_valid():
            serializers.save()
            return Response(inf,status=status.HTTP_200_OK)
        else:
            return Response(serializers.errors, status=status.HTTP_400_BAD_REQUEST)