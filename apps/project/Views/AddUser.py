from django.shortcuts import render

# Create your views here.
from apps.project.models import *
from apps.project.serializer import *

from rest_framework import status, generics
from rest_framework.response import Response
from datetime import datetime

class AddUser(generics.GenericAPIView):
    serializer_class = UserProjectSerializer
    queryset = UsuariosProyecto.objects.all()
    def get(self, request, project):
        users = UsuariosProyecto.objects.filter(proyecto=project)
        serializer = UserProjectSerializer(users, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, project):
        inf = request.data.copy()
        add ={
            'proyecto':project,
            'fecha_creacion': datetime.now(),
        }
        inf.update(add)
        serializers = UserProjectSerializer(data = inf)
        if serializers.is_valid():
            serializers.save()
            return Response(inf,status=status.HTTP_200_OK)
        else:
            return Response(serializers.errors, status=status.HTTP_400_BAD_REQUEST)