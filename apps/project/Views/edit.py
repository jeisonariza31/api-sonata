from django.shortcuts import render

# Create your views here.
from apps.project.models import *
from apps.project.serializer import *

from rest_framework import status, generics
from rest_framework.response import Response

class EditProject(generics.GenericAPIView):
    serializer_class= EditProjectSerializer
    queryset =  proyecto.objects.all()
    def get(self, request, id):
        project = proyecto.objects.get(id = id)
        serializer = ProyectoSerializer(project)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id):
            project = proyecto.objects.get(id = id)
            serializer = ProyectoSerializer(project, request.data)  
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)