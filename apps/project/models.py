from django.db import models
from apps.user.models import *
from apps.phase.models import *
from apps.RolProject.models import *


# Create your models here.
class proyecto(models.Model):
    id = models.AutoField(primary_key = True)
    nombre = models.CharField(max_length = 100, unique = True)
    descripcion = models.TextField(max_length = 300)
    estado = models.CharField(max_length = 20)
    estado_entrega = models.CharField(max_length = 30)
    fecha_inicio = models.DateField()
    fecha_plan_fin = models.DateField()
    fecha_fin = models.DateField(null= True, blank=True)
    fase = models.ForeignKey(fase, on_delete = models.CASCADE, default=1)
    usuario_cli = models.ForeignKey(usuario, on_delete = models.CASCADE, related_name='cliente', default=2)
    usuario_creacion = models.ForeignKey(usuario, on_delete = models.CASCADE, related_name='creacion', default=1)
    fecha_creacion = models.DateField(auto_now=True)
    consultores = models.ManyToManyField(usuario, through='UsuariosProyecto')

    def __str__(self):
        return self.nombre

class UsuariosProyecto(models.Model):
    id = models.AutoField(primary_key = True)
    usuario = models.ForeignKey(usuario, on_delete=models.CASCADE)
    proyecto = models.ForeignKey(proyecto, on_delete=models.CASCADE)
    rol = models.ForeignKey(RolProyecto, on_delete=models.CASCADE, default=1)
    fecha_creacion = models.DateTimeField(auto_now_add=True)

    
class ContactoCliente(models.Model):
    id = models.AutoField(primary_key = True)
    area = models.CharField(max_length = 50)
    telefono = models.CharField(max_length = 10)
    direccion = models.CharField(max_length = 200)
    proyecto = models.ForeignKey(proyecto, on_delete = models.CASCADE)

    def __str__(self):
        return self.proyecto