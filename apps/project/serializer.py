from .models import *
from rest_framework import serializers
from apps.user.serializer import *


""" default serializer """

class ProyectoSerializer(serializers.ModelSerializer):
    class Meta:
        model = proyecto
        fields = '__all__'

class ContactoClienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContactoCliente
        fields = '__all__'

class UsuariosProyectoSerializer(serializers.ModelSerializer):
    class Meta:
        model = UsuariosProyecto
        fields = '__all__'

""" custom serializer """

class ProjectSerializer(serializers.ModelSerializer):
    nombre = serializers.CharField(max_length = 100)
    descripcion = serializers.CharField(style={'base_template': 'textarea.html'},max_length = 300)
    fecha_inicio = serializers.DateField()
    fecha_plan_fin = serializers.DateField()
    usuario_cli = models.ForeignKey(usuario, on_delete = models.CASCADE)
    usuario_creacion = models.ForeignKey(usuario, on_delete = models.CASCADE)
    class Meta:
        model = proyecto
        fields = ['nombre','descripcion','fecha_inicio','fecha_plan_fin','usuario_cli','usuario_creacion']

class EditProjectSerializer(serializers.ModelSerializer):
    nombre = serializers.CharField(max_length = 100)
    descripcion = serializers.CharField(style={'base_template': 'textarea.html'},max_length = 300)
    fecha_inicio = serializers.DateField()
    fecha_plan_fin = serializers.DateField()
    estado = serializers.CharField(max_length = 20)
    estado_entrega = serializers.CharField(max_length = 30)
    usuario_cli = models.ForeignKey(usuario, on_delete = models.CASCADE)
    usuario_creacion = models.ForeignKey(usuario, on_delete = models.CASCADE)
    class Meta:
        model = proyecto
        fields = ['nombre','descripcion','fecha_inicio','fecha_plan_fin', 'estado', 'estado_entrega','usuario_cli','usuario_creacion']


class UserProjectSerializer(serializers.ModelSerializer):
    usuario = models.ForeignKey(usuario, on_delete=models.CASCADE)
    proyecto = models.ForeignKey(proyecto, on_delete=models.CASCADE)
    rol = models.ForeignKey(RolProyecto, on_delete=models.CASCADE, default=1)
    class Meta:
        model = UsuariosProyecto
        fields = ['usuario','proyecto','rol']