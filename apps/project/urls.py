from django.urls import path

from .Views.AddUser import *
from .Views.project import *
from .Views.edit import *

urlpatterns = [
    path('', ProjectListAdmin.as_view(), name='project'),
    path('/<int:id>', EditProject.as_view(), name = 'edit'),
    path('/<int:project>/user/', AddUser.as_view(), name = 'adduser'),
    #path('/<int:project>/user/<int:user>', EditUserProject, name='edit user'),
]
