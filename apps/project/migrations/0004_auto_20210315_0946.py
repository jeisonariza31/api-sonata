# Generated by Django 3.1.7 on 2021-03-15 14:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0003_auto_20210312_1725'),
    ]

    operations = [
        migrations.AlterField(
            model_name='proyecto',
            name='fecha_fin',
            field=models.DateField(blank=True, default='', null=True),
        ),
    ]
