from .models import *
from rest_framework import serializers

""" default serializer """

class ContactProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContactoProyecto
        fields = '__all__'

class NewContactProjectSerializer(serializers.ModelSerializer):
    area = serializers.CharField(max_length = 100)
    telefono = serializers.CharField(max_length = 10)
    direccion = serializers.CharField(max_length = 200)
    usuario_creacion = models.ForeignKey(usuario, on_delete = models.CASCADE)

    class Meta:
        model = ContactoProyecto
        fields = ['area', 'telefono', 'direccion', 'usuario_creacion']

class EditContactProjectSerializer(serializers.ModelSerializer):
    area = serializers.CharField(max_length = 100)
    telefono = serializers.CharField(max_length = 10)
    direccion = serializers.CharField(max_length = 200)

    class Meta:
        model = ContactoProyecto
        fields = ['area', 'telefono', 'direccion']