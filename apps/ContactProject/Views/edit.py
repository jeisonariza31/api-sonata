from django.shortcuts import render

from apps.ContactProject.models import *
from apps.ContactProject.serializer import *

from rest_framework import status, generics
from rest_framework.response import Response

class EditContactProject(generics.GenericAPIView):
    serializer_class = EditContactProjectSerializer
    queryset = ContactoProyecto.objects.all()

    def get (self, request, project, contact):
        contact = ContactoProyecto.objects.get(id = contact)
        serializer = ContactProjectSerializer(contact)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put (self, request, project, contact):
        contact = ContactoProyecto.objects.get(id = contact)
        serializer = EditContactProjectSerializer(contact, request.data)  
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)