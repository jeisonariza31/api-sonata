from django.shortcuts import render

from apps.ContactProject.models import *
from apps.ContactProject.serializer import *

from rest_framework import status, generics
from rest_framework.response import Response
from datetime import datetime

class ContactProject(generics.GenericAPIView):
    serializer_class = NewContactProjectSerializer
    queryset = ContactoProyecto.objects.all()

    def get (self, request, project):
        contacts = ContactoProyecto.objects.filter(proyecto = project)
        serializer = ContactProjectSerializer(contacts, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post (self, request, project):
        inf = request.data.copy()
        add ={
            'proyecto':project,
            'fecha_creacion': datetime.now(),
        }
        inf.update(add)
        serializers = ContactProjectSerializer(data = inf)
        if serializers.is_valid():
            serializers.save()
            return Response(inf,status=status.HTTP_200_OK)
        else:
            return Response(serializers.errors, status=status.HTTP_400_BAD_REQUEST)