from django.db import models
from apps.project.models import *
from apps.user.models import *

# Create your models here.

class ContactoProyecto(models.Model):
    id = models.AutoField(primary_key = True)
    area = models.CharField(max_length = 100)
    telefono = models.CharField(max_length = 10)
    direccion = models.CharField(max_length = 200)
    proyecto = models.ForeignKey(proyecto, on_delete = models.CASCADE)
    usuario_creacion = models.ForeignKey(usuario, on_delete = models.CASCADE)
    fecha_creacion = models.DateField(auto_now_add= True)