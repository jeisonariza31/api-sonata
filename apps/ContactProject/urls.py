from django.urls import path
from .Views.ContactProject import *
from .Views.edit import *

urlpatterns = [
    path('/<int:project>/contact', ContactProject.as_view(), name = 'ContactProject'),
    path('/<int:project>/contact/<int:contact>',EditContactProject.as_view(), name = 'EditContact'),
]
