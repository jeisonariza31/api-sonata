from django.apps import AppConfig


class ContactprojectConfig(AppConfig):
    name = 'ContactProject'
